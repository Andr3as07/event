package tk.andr3as07.util.event;

import java.lang.annotation.*;

/**
 * Defines a method as a Event Handler
 * @author Andr3as07
 * @since 0.1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface EventHandler {
	Class<? extends Event> type() default Event.class;
}