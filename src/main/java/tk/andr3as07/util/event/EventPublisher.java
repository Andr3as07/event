package tk.andr3as07.util.event;

import java.lang.reflect.Method;

/**
 * This Class is used to Handle the event Publication
 * @author Andr3as07
 * @since 0.1.0
 */
public class EventPublisher {

	/**
	 * Fires the event in a new Thread
	 * @param event the Event to be fired
	 */
	public static void fireAsync(final Event event) {
		new Thread() {
			@Override
			public void run() {
				fireSync(event);
			}
		}.start();
	}

	/**
	 * Is called from the fire method, Fires the Event actually and prints a Stacktrace if a error occurs
	 * @param event the Event to be fired
	 */
	private static void fireSync(final Event event) {
		for (final Class<?> handler : HandlerRegistry.getHandlers()) {
			final Method[] methods = handler.getMethods();

			for (final Method method : methods) {
				final EventHandler annotation = method.getAnnotation(EventHandler.class);

				if (annotation != null) {
					if (!annotation.type().isAssignableFrom(event.getClass()))
						continue;

					final Class<?>[] params = method.getParameterTypes();

					if (params.length != 1)
						continue;

					// Catch Runtime Exceptions
					try {
						method.invoke(handler.newInstance(), event);
					} catch (final Exception ex) {
						ex.printStackTrace();
					}
				}
			}
		}
	}

}