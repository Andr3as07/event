package tk.andr3as07.util.event;

import java.util.*;

/**
 * This Class handles all the adding and removing of EventHandlers
 * @author Andr3as07
 * @since 0.1.0
 */
public class HandlerRegistry {

	/**
	 * The static list of Classes containing EventHandlers
	 */
	private static List<Class<?>> handlers = new ArrayList<Class<?>>();

	/**
	 * Checks if the Class is already registered as a HandlerClass
	 * @param cls the Class containing EventHandlers
	 * @return true if the Class is registered
	 */
	public static boolean exists(final Class<?> cls) {
		return handlers.contains(cls);
	}

	/**
	 * Gets all HandlerClasses currently registered
	 * @return the HandlerClasses
	 */
	public static List<Class<?>> getHandlers() {
		return Collections.unmodifiableList(handlers);
	}

	/**
	 * Adds a Class to the list of HandlerClasses
	 * <p>
	 * Checks if the Class exists first.
	 * If false it adds the Class it the list
	 * @param cls the Class containing EventHandlers
	 * @return if the adding process was successful
	 */
	public static boolean register(final Class<?> cls) {
		if (exists(cls))
			return false;
		handlers.add(cls);
		return true;
	}

	/**
	 * Removes the Class from the list of HandlerClasses
	 * <p>
	 * Checks if the Class exists first
	 * If true it removes the Class from the list
	 * @param cls the Class containing EventHandlers
	 * @return if the removing process was successful
	 */
	public static boolean remove(final Class<?> cls) {
		if (!exists(cls))
			return false;
		handlers.remove(cls);
		return true;
	}
}