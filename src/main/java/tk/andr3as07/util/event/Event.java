package tk.andr3as07.util.event;

/**
 * The basic layout for any Event
 * @author Andr3as07
 * @version 0.1.0
 */
public class Event {

	/**
	 * Holds the next value output by the next() function
	 */
	private static long next = Long.MIN_VALUE;

	private static long next() {
		return ++next;
	}

	/**
	 * The ID of the Event
	 */
	private final long eventID;

	/**
	 * Creates a new Event
	 */
	protected Event() {
		this.eventID = next();
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		final Event other = (Event) obj;
		if (this.eventID != other.eventID)
			return false;
		return true;
	}

	public final long getEventID() {
		return this.eventID;
	}

	/**
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		int result = 1;
		result = 31 + (int) (this.eventID ^ this.eventID >>> 32);
		return result;
	}

}