package tk.andr3as07.util.event;

import junit.framework.*;

public class EventTest extends TestCase {

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(EventTest.class);
	}

	/**
	 * Create the test case
	 *
	 * @param testName
	 *            name of the test case
	 */
	public EventTest(final String testName) {
		super(testName);
	}

	public void testNext() {
		Event e0 = new Event(),
				e1 = new Event();
		assertTrue(e0.getEventID() + 1 == e1.getEventID());
	}

	public void testEquals() {
		Event e0 = new Event(),
				e1 = new Event();
		Event e2 = e0;
		
		assertFalse(e0 == e1);
		assertTrue(e0 == e2);
	}

}
