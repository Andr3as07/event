package tk.andr3as07.util.event;

import junit.framework.*;

public class HandlerRegistryTest extends TestCase {

	private void clear() {
		HandlerRegistry.remove(this.getClass());
	}
	
	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(HandlerRegistryTest.class);
	}

	/**
	 * Create the test case
	 *
	 * @param testName
	 *            name of the test case
	 */
	public HandlerRegistryTest(final String testName) {
		super(testName);
	}

	public void testRegister() {
		clear();

		assertTrue(HandlerRegistry.getHandlers().size() == 0);

		HandlerRegistry.register(this.getClass());
		assertTrue(HandlerRegistry.getHandlers().size() == 1);
		assertTrue(HandlerRegistry.getHandlers().get(0) == this.getClass());
		
		clear();
	}

	public void testRemove() {
		clear();

		assertTrue(HandlerRegistry.getHandlers().size() == 0);

		HandlerRegistry.register(this.getClass());
		assertTrue(HandlerRegistry.getHandlers().size() == 1);
		
		HandlerRegistry.remove(this.getClass());
		assertTrue(HandlerRegistry.getHandlers().size() == 0);
		
		clear();
	}

	public void testExists() {
		clear();

		assertTrue(HandlerRegistry.getHandlers().size() == 0);
		assertFalse(HandlerRegistry.exists(this.getClass()));
		HandlerRegistry.register(this.getClass());
		assertTrue(HandlerRegistry.exists(this.getClass()));

		HandlerRegistry.remove(this.getClass());
		assertFalse(HandlerRegistry.exists(this.getClass()));

		clear();
	}

	public void testGetHandlers() {
		clear();

		HandlerRegistry.register(this.getClass());
		assertTrue(HandlerRegistry.getHandlers().get(0).getName() == this.getClass().getName());

		clear();
	}
}